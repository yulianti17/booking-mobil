/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * 
 */
public class H2Database {

    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:tcp://localhost/~/test";

    static final String USER = "sa";
    static final String PASSWORD = "";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            // Register JDBC Driver
            Class.forName(JDBC_DRIVER);

            // Open a connection
            System.out.println("Connecting to a database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);

            // Execute queries
            System.out.println("Drop table if exists...");
            stmt = conn.createStatement();
            String sql = "DROP TABLE IF EXISTS customer, booking, car";
            stmt.executeUpdate(sql);

            System.out.println("Create customer table...");
            sql = "CREATE TABLE IF NOT EXISTS customer "
                    + "(id INT AUTO_INCREMENT, "
                    + "name VARCHAR(255) NULL, "
                    + "PRIMARY KEY (id))";
            stmt.executeUpdate(sql);
            System.out.println("Table customer created...");

            System.out.println("Create car table...");
            sql = "CREATE TABLE IF NOT EXISTS car "
                    + "(id INT AUTO_INCREMENT, "
                    + "licenceNo VARCHAR(255) NULL, "
                    + "category VARCHAR(255) NULL, "
                    + "PRIMARY KEY (id))";
            stmt.executeUpdate(sql);
            System.out.println("Table car created...");

            System.out.println("Create booking table...");
            sql = "CREATE TABLE IF NOT EXISTS booking "
                    + "(id INT AUTO_INCREMENT, "
                    + "start DATETIME, "
                    + "end DATETIME, "
                    + "category VARCHAR(255) NULL, "
                    + "pickedUp INTEGER, "
                    + "customerName VARCHAR(255) NULL, "
                    + "returned DATETIME, "
                    + "PRIMARY KEY (id))";
            stmt.executeUpdate(sql);
            System.out.println("Table booking created...");

            // Clean-up environment
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            } // nothing we can do 

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            } //end finally try 
        }

        System.out.println("Finish!!!");
    }

}
