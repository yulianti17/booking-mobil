/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Customer;

/**
 *
 * @author  yulianti.smtp
 */
public class CustomerModel extends AbstractModel<Customer> {
    
    public CustomerModel() {
        super(Customer.class);
    }
    
}
