/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Booking;
import entity.Car;
import entity.Customer;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

/**
 *
 * @author  yulianti.smtp
 */
public class Test {
    public static void main(String[] args) {
        BookingModel bm = new BookingModel();
        Booking b = new Booking();
        b.setId(1);
        b.setStart(Date.from(LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC)));
        b.setEnd(Date.from(LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC)));
        b.setCategory("economy");
        b.setCustomername("Juli");
        b.setPickedup(1);
        bm.create(b);
        
        CarModel cm = new CarModel();
        Car c = new Car();
        c.setId(1);
        c.setCategory("economy");
        c.setLicenceno("1234");
        cm.create(c);
        
        CustomerModel cusm = new CustomerModel();
        Customer cus = new Customer();
        cus.setId(1);
        cus.setName("Juli");
        cusm.create(cus);
    }
}
