/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Booking;

/**
 *
 * @author  yulianti.smtp
 */
public class BookingModel extends AbstractModel<Booking> {
    
    public BookingModel() {
        super(Booking.class);
    }
    
}
