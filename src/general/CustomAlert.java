/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package general;

/**
 *
 * @author  yulianti.smtp
 */
public class CustomAlert {
    
    public static final String ALERT_SUCCESS = "success";
    public static final String ALERT_FAILED = "failed"; 
    
    public static void setAlert(String message, String alertType) {
        javafx.scene.control.Alert alert = null;

        if (alertType == ALERT_SUCCESS) {
            alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText(message);
        } else {
            alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText(message);
        }

        alert.showAndWait();
    }
    
}
