/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package general;

/**
 *
 * @author yulianti.smtp
 */
public enum CatET {
    economy, standard, luxus;
    
    public String toString(){
        switch(this){
            case economy:
                return "Economy";
            case standard:
                return "Standard";
            case luxus:
                return "Luxus";
            default : return "Nothing";
        }
    }
    
}
