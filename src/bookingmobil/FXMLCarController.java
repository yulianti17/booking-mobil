/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookingmobil;

import entity.Car;
import general.CatET;
import general.CustomAlert;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
/*import javafx.scene.control.Alert;*/
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import model.CarModel;

/**
 * FXML Controller class
 *
 * @author Yulianti.smtp
 */
public class FXMLCarController implements Initializable {

    @FXML
    private TableView<Car> tv_tabelcar;
    @FXML
    private TableColumn tc_licenceno;
    @FXML
    private TableColumn tc_category;
    @FXML
    private Button btn_addcar;
    @FXML
    private TextField tf_licencenumber;
    @FXML
    private ComboBox cb_category; 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        clear();
        initializeTable();
        setComboBoxValue();
    } 

    private void initializeTable() {
        ObservableList<Car> data = FXCollections.observableArrayList();
        CarModel cm = new CarModel();
        
        for (Car c : cm.findAll()) {
            data.add(c);
        }
        
        tc_licenceno.setCellValueFactory(new PropertyValueFactory<>("licenceno"));
        tc_category.setCellValueFactory(new PropertyValueFactory<>("category"));
        
        tv_tabelcar.getColumns().clear();
        tv_tabelcar.setItems(data);
        tv_tabelcar.getColumns().addAll(tc_licenceno, tc_category);
    }
    
    private void setComboBoxValue() {
        cb_category.getItems().setAll(CatET.values());
    }
    
    @FXML
    private void HandleButtonAddCar(ActionEvent event) {
        CarModel cm = new CarModel();
        
        if (tf_licencenumber.getText().trim().equalsIgnoreCase("") ||
                cb_category.getValue() == null) {
            System.out.println("Test");
            CustomAlert.setAlert("Data not complete", CustomAlert.ALERT_FAILED);
        } else {
            Car c = new Car();
            c.setCategory(cb_category.getSelectionModel().getSelectedItem().toString());
            c.setLicenceno(tf_licencenumber.getText());
            cm.create(c);
            
            clear();
            tv_tabelcar.getItems().add(c);
        }
    }
    
    private void clear() {
        tf_licencenumber.setText("");
        cb_category.setValue(null);
    }
    
}
