/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookingmobil;

import entity.Booking;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.BookingModel;

/**
 *
 * @author Yulianti.smtp
 */
public class FXMLHomeController implements Initializable {
    
    @FXML
    private TableView<Booking> tv_tabel;
    @FXML
    private TableColumn tc_startdate;
    @FXML
    private TableColumn tc_enddate;
    @FXML
    private TableColumn tc_category;
    @FXML
    private TableColumn tc_pickedup;
    @FXML
    private TableColumn tc_return;
    @FXML
    private TableColumn tc_customer;
    @FXML
    private Button btn_car;
    @FXML
    private Button btn_Customer;
    @FXML
    private Button btn_booking;
    @FXML
    private Button btn_refresh;
    
    ObservableList<Booking> data = null;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        initializeTable();
    }    

    public void initializeTable() {
        data = FXCollections.observableArrayList();
        BookingModel bm = new BookingModel();
        
        for (Booking b : bm.findAll()) {
            data.add(b);
        }
        
        tc_startdate.setCellValueFactory(new PropertyValueFactory<>("start"));
        tc_enddate.setCellValueFactory(new PropertyValueFactory<>("end"));
        tc_category.setCellValueFactory(new PropertyValueFactory<>("category"));
        tc_pickedup.setCellValueFactory(new PropertyValueFactory<>("pickedup"));
        tc_return.setCellValueFactory(new PropertyValueFactory<>("returned"));
        tc_customer.setCellValueFactory(new PropertyValueFactory<>("customername"));
        
        tv_tabel.getColumns().clear();
        tv_tabel.setItems(data);
        tv_tabel.getColumns().addAll(tc_startdate, tc_enddate, tc_category, tc_pickedup, tc_return, tc_customer);
    }
    
    @FXML
    private void HandleButtonCar(ActionEvent event) throws IOException {
        Parent carParent = FXMLLoader.load(getClass().getResource("FXMLCar.fxml"));

        Scene carScene = new Scene(carParent);
        Stage carStage = new Stage();
        
        carStage.setScene(carScene);
        carStage.show();
    }

    @FXML
    private void HandleButtonCustomer(ActionEvent event) throws IOException {
        Parent customerParent = FXMLLoader.load(getClass().getResource("FXMLCustomer.fxml"));

        Scene customerScene = new Scene(customerParent);
        Stage customerStage = new Stage();
        
        customerStage.setScene(customerScene);
        customerStage.show();
    }

    @FXML
    private void HandleButtonBooking(ActionEvent event) throws IOException {
        Parent bookingParent = FXMLLoader.load(getClass().getResource("FXMLBooking.fxml"));

        Scene bookingScene = new Scene(bookingParent);
        Stage bookingStage = new Stage();
        
        bookingStage.setScene(bookingScene);
        bookingStage.show();
    }

    @FXML
    private void HandleButtonRefresh(ActionEvent event) {
        initializeTable();
    }
    
    @FXML
    private void HandleReturn(MouseEvent event) throws IOException {
        if (event.getClickCount() == 2) {
            int row = tv_tabel.getSelectionModel().getSelectedIndex();
            Booking booking = data.get(row);

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLReturn.fxml"));
            Parent detailViewParent = loader.load();
            
            Scene detailViewScene = new Scene(detailViewParent);
            
            FXMLReturnController controller = loader.getController();
            controller.initData(booking);
            
            Stage returnViewStage = new Stage();
            returnViewStage.setScene(detailViewScene);
            returnViewStage.show();
        }
    }
    
}
