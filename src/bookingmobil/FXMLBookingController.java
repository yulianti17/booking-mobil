/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookingmobil;

import entity.Booking;
import entity.Car;
import entity.Customer;
import general.CustomAlert;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import model.BookingModel;
import model.CarModel;
import model.CustomerModel;

/**
 * FXML Controller class
 *
 * @author Yulianti.smtp
 */
public class FXMLBookingController implements Initializable {

    @FXML
    private DatePicker dp_startdate;
    @FXML
    private ComboBox cb_customer;
    @FXML
    private Button btn_booking;
    @FXML
    private ComboBox cb_licencenumber;
    @FXML
    private DatePicker dp_enddate;
    @FXML
    private TextField tf_category;

    ObservableList<Car> carData = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tf_category.setDisable(true);
        setComboBoxValue();
    }

    private void setComboBoxValue() {
        CustomerModel cusMod = new CustomerModel();
        ObservableList<String> customerName = FXCollections.observableArrayList();
        ObservableList<String> licenceNumber = FXCollections.observableArrayList();

        for (Customer cus : cusMod.findAll()) {
            customerName.add(cus.getName());
        }

        cb_customer.getItems().setAll(customerName);

        CarModel cm = new CarModel();
        carData = FXCollections.observableArrayList();

        for (Car c : cm.findAll()) {
            carData.add(c);
            licenceNumber.add(c.getLicenceno());
        }

        cb_licencenumber.getItems().setAll(licenceNumber);
    }

    @FXML
    private void HandleButtonBooking(ActionEvent event) {
        if (tf_category.getText().equalsIgnoreCase("")
                || cb_licencenumber.getValue() == null
                || cb_customer.getValue() == null
                || dp_startdate.getValue() == null
                || dp_enddate.getValue() == null) {
            CustomAlert.setAlert("Data not complete", CustomAlert.ALERT_FAILED);
        } else {
            BookingModel bm = new BookingModel();
            Booking b = new Booking();
            b.setStart(Date.valueOf(dp_startdate.getValue()));
            b.setEnd(Date.valueOf(dp_enddate.getValue()));
            b.setCategory(tf_category.getText());
            b.setCustomername(cb_customer.getSelectionModel().getSelectedItem().toString());
            b.setPickedup(1);
            bm.create(b);

            clear();
        }
    }

    @FXML
    private void HandleComboLicence(ActionEvent event) {
        tf_category.setText("");

        if (cb_licencenumber.getValue() != null) {
            for (Car c : carData) {
                if (c.getLicenceno().equalsIgnoreCase(cb_licencenumber.getValue().toString())) {
                    tf_category.setText(c.getCategory());
                }
            }
        }
    }

    private void clear() {
        dp_startdate.setValue(null);
        dp_enddate.setValue(null);
        cb_customer.setValue(null);
        cb_licencenumber.setValue(null);
        tf_category.setText("");
    }

}
