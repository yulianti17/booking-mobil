package bookingmobil;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import entity.Customer;
import general.CustomAlert;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import model.CustomerModel;

/**
 * FXML Controller class
 *
 * @author Yulianti.smtp
 */
public class FXMLCustomerController implements Initializable {

    @FXML
    private TableView<Customer> tv_customer;
    @FXML
    private TableColumn tc_nama;
    @FXML
    private TextField tf_nama;
    @FXML
    private Button btn_add;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        clear();
        initializeTable();
    }    

    private void initializeTable() {
        ObservableList<Customer> data = FXCollections.observableArrayList();
        CustomerModel cm = new CustomerModel();
        
        for (Customer c : cm.findAll()) {
            data.add(c);
        }
        
        tc_nama.setCellValueFactory(new PropertyValueFactory<>("name"));
        
        tv_customer.getColumns().clear();
        tv_customer.setItems(data);
        tv_customer.getColumns().addAll(tc_nama);
    }
    
    @FXML
    private void HandleButtonAdd(ActionEvent event) {
        CustomerModel cm = new CustomerModel();
        
        if (tf_nama.getText().trim().equalsIgnoreCase("")) {
            System.out.println("Test");
            CustomAlert.setAlert("Data not complete", CustomAlert.ALERT_FAILED);
        } else {
            Customer c = new Customer();
            c.setName(tf_nama.getText());
            cm.create(c);
            
            clear();
            tv_customer.getItems().add(c);
        }
    }
    
    private void clear() {
        tf_nama.setText("");
    }
    
}
