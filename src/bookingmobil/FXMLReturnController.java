/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookingmobil;

import entity.Booking;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import model.BookingModel;

/**
 * FXML Controller class
 *
 * @author Yulianti.smtp
 */
public class FXMLReturnController implements Initializable {

    @FXML
    private Button btn_return;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    private Booking booking = null;
    
    public void initData(Booking booking){
        this.booking = booking;
    }

    @FXML
    private void HandleReturn(ActionEvent event) {
        ButtonType okButton = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        ButtonType noButton = new ButtonType("NO", ButtonBar.ButtonData.NO);
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Return");
        alert.setContentText("Do you want to return?");
        
        alert.getButtonTypes().setAll(okButton, noButton);
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == okButton) {
            this.booking.setReturned(Date.valueOf(LocalDate.now()));
            
            BookingModel bm = new BookingModel();
            bm.update(this.booking);
        }
    }
}
